#!/bin/bash
# Author:        Chunhua Shen {chhshen@gmail.com}, Adelaide
# Creation:      Wednesday 19/10/2011 10:06.
# Last Revision: Saturday 26/11/2011 15:20.
#
# convert all the media files in a directory into DIVX, OGV or MPEG2


FFMPEG=ffmpeg


if [ -z $1 ]; then
    ext=avi
else
    ext=$1
fi

# string length
if [ ${#ext} == 0 ]; then
    ext=avi
fi

if [ -z $2 ]; then
    format=ogv
else
    format=$2
fi


for i in `ls -1 *$ext`
do

    echo "- converting ... $i"

    fname=`basename $i .avi`

       case "$format" in
           ogv)
                $FFMPEG  -i $i  -acodec libvorbis -ac 2 -ab 96k -ar 44100   -b  345k \
                $fname.ogv
                ;;
            divx)
                # DIVX
                $FFMPEG  -i $i -vcodec msmpeg4v2 $fname"_divx.avi"
                ;;
            mp4)

                BITRATE=1200k
                if [ ! -z $3 ]; then
                    BITRATE="$3"
                    echo "You set the MP4 bitrate to be "$BITRATE
                fi


                ffmpeg -i $i -acodec libfaac -ab 128k -vcodec mpeg4 -b \
                $BITRATE -mbd 2 -flags +mv4+aic -trellis 2 -cmp 2 -subcmp \
                2  -metadata title=$fname $fname.mp4
                ;;
            *)
                echo " $format: Not supported format."
                echo " supported format: ogv, divx, mp4"
                ;;
        esac
done








