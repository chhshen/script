#!/bin/bash

if [ $1 == 1 ];
then
# print author
    cat template.tex | grep author | tail -1 | sed 's/{/|  /' | sed 's/}/ /' \
       |  awk -F'|' '{print $2 }'


else
# print title
    cat template.tex | grep title | head -1 | sed 's/{/|  /' | sed 's/}/ /' \
     |  awk -F'|' '{print $2 }'

fi
