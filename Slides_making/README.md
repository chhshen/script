# Making slides (Beamer LaTeX and HTML5) with Markdown

Follow the Markdown format; and only change the header (author, title etc) in
template.tex.

Main content of your talk in in talk.md

You only need to modify these two files; and compile by typing `make'

Chunhua Shen, Nov 2013
