#!/bin/bash
# Chunhua Shen, 2 April 2015 @Adelaide
# This script automatically download images from Google Image, by querying the keywords listed in a file
#


function checkcmd()
{
  type $1  > /dev/null || echo "-- Please install $1!" || exit -2
}

function trim()
{
    local var="$*"
    var="${var#"${var%%[![:space:]]*}"}"   # remove leading whitespace characters
    var="${var%"${var##*[![:space:]]}"}"   # remove trailing whitespace characters
    echo -n "$var"
}

function checkinput()
{
  [ -z "$1" ] && \
    echo "- Please provide an input file that stores the query list." && \
    echo "- Usage: " && \
    echo "      $0 input_file" && \
    exit -2
}

checkinput $*

gnu_sed=sed

checkcmd sed
checkcmd tr
checkcmd curl



list_file=$1
[[ ! -f $list_file ]] && echo "- Input file: $list_file not found!" && exit -5


while read Q
do
    # trim whitespace & newlines
    Q1=`trim $Q`
    Query=`echo $Q1 | $gnu_sed 's/ /+/g'`
    len=${#Query}
    [[ $len == 0 ]] && continue


    echo "- Processing:  $Q1"
    # echo $Query


    mkdir -p ./URLs

    # Google search string. image size > 88x600; type of image: face;
    # str="https://www.google.com/search?as_st=y&tbm=isch&as_q=$Query&as_epq=&as_oq=&as_eq=&cr=&as_sitesearch=&safe=images&tbs=isz:lt,islt:svga,itp:face&gws_rd=ssl"

    # Baidu image search
    str="http://image.baidu.com/i?tn=baiduimage&ps=1&ct=201326592&lm=-1&cl=2&nc=1&ie=utf-8&word=$Query#z=2&pn=&ic=0&st=-1&face=0&s=0&lm=-1"


    #
    # return the first 100 hits. Using the default lynx as the useragent will return about 10 hits
    #
    curl -A  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.112 Safari/534.30" \
      $str > _out      2> /dev/null


    [ $? -ne 0 ] && continue

    cat _out | grep objURL | awk -F "\"" '{ print $4 }' \
               > ./URLs/"$Query".txt


    img_dir=./Images/"$Query"
    mkdir -p $img_dir
    echo "--- downloading images"

    mkdir -p _temp
    cd _temp

    IND=0
    while read img_url
    do
        let "IND++"

        curl -#  --remote-name  $img_url
        [ $? -ne 0 ] && continue

        f=`ls -1 -t | head -1`
        mv -f "$f"  ../$img_dir/$IND-"$f"

        if [ $? -ne 0 ]
        then
            cd .. && rm -fr _temp
            mkdir -p ./_temp
            cd ./_temp
            continue
        fi

    done < ../URLs/"$Query".txt

    cd ..
done < "$list_file"


rm -f _out

