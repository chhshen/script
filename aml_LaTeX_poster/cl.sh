#!/bin/bash


F=poster

latex $F
bibtex $F

latex $F
bibtex $F

dvips $F
pstopdf $F.ps

rm -f *log *aux *bbl *blg *dvi
