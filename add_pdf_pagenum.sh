#!/bin/bash


echo "Bash script for adding page numbers to a PDF using PDFTK"

PDFDIR=PDF_`date | sed -e 's/ /_/g' | sed -e 's/:/_/g'`


PWD1=`pwd`
echo $PWD1

if [ -z $1 ]; then
    echo "Please tell me the input PDF file!"
    exit
fi
INPUTPDF0="$1"

if [ ! -f $INPUTPDF0 ]; then
    echo "Input PDF file not found! Please double check!"
    exit
fi


echo "Checking for dependencies"
if ! which pdftk > /dev/null; then
    echo "Dependencies not met: pdftk not found"
    echo "Please install pdftk and try again"
    exit
fi

if ! which pdflatex > /dev/null; then
    echo "Dependencies not met: pdflatex not found"
    echo "Please install pdflatex and try again"
    exit
fi


echo "Preparing System"


mkdir -p /tmp/$PDFDIR


cp -f $INPUTPDF0  /tmp/$PDFDIR


INPUTPDF=`basename $INPUTPDF0`

cd /tmp/$PDFDIR

echo "Calculating Page Numbers ..."
#If you want to have some pages to not be numbered simply have this script move them to ./temp at this point

PAGE=$(pdfinfo $INPUTPDF | grep "Pages")
PAGE=$(echo $PAGE | cut -c 8-)


echo "Creating Page Number file for "$PAGE" pages"
(
printf '\\documentclass[10pt]{article}\n'
printf '\\usepackage{multido,fancyhdr}\n'
printf '\\usepackage[hmargin=0cm,vmargin=1.5cm,nohead,nofoot]{geometry}\n'
printf '\\pagestyle{fancy}\n'
printf '\\fancyhead{}\n'
printf '\\fancyfoot{}\n'
printf '\\renewcommand{\\headrulewidth}{0pt}\n'
printf '\\renewcommand{\\footrulewidth}{0pt}\n'
printf '\\rfoot{$\cdot$ \it\\thepage$/$'$PAGE' $\cdot$ \\hspace{1cm} }\n'
printf '\\begin{document}\n'
printf '\\multido{}{'$PAGE'}{\\vphantom{x}\\newpage}\n'
printf '\\end{document}'
) >./numbers.tex



pdflatex -interaction=batchmode numbers.tex 1>/dev/null


echo "Bursting PDF's"
pdftk $INPUTPDF   burst output prenumb_burst_%03d.pdf
pdftk numbers.pdf burst output number_burst_%03d.pdf


echo "Now adding Page Numbers:"

for (( i = 1; i <= $PAGE; i++ )); do

    iii=i
    length=${#i}
    if [[ $length -eq 1 ]]; then
         iii=00$i
    fi

    if [[ $length -eq 2 ]]; then
         iii=0$i
    fi

    pdftk prenumb_burst_$iii.pdf background number_burst_$iii.pdf \
    output ./numbered_tomerge_$iii.pdf;
done

echo "Merging *.pdf files"

PDF2Merge=`ls -1 ./numbered_tomerge_*.pdf`

pdftk $PDF2Merge cat output ./numbered.pdf

echo "Optimizing PDF file"
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer \
-dNOPAUSE \-dQUIET -dBATCH -sOutputFile=numbered_bloat.pdf  numbered.pdf 2>/dev/null


OUTPDF="$PWD1"/`basename $INPUTPDF0  .pdf`"_numbered.pdf"
cp -i ./numbered_bloat.pdf $OUTPDF

echo "Cleaning Up"

rm -fr  /tmp/$PDFDIR


echo "The numbered PDF file is $OUTPDF"


#
#EoF
#




















